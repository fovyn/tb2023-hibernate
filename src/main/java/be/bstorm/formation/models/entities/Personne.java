package be.bstorm.formation.models.entities;


import be.bstorm.formation.models.BaseEntity;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public class Personne extends BaseEntity<Long> {

    @Basic(fetch = FetchType.LAZY)
    private String lastname;

    @ManyToMany(cascade = {CascadeType.PERSIST})
    private List<Adresse> adresses = newArrayList();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Personne personne = (Personne) o;
        return Objects.equal(getId(), personne.getId()) && Objects.equal(lastname, personne.lastname) && Objects.equal(adresses, personne.adresses);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId(), lastname, adresses);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", getId())
                .add("lastname", lastname)
                .add("adresses", adresses)
                .toString();
    }
}
