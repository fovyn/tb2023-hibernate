package be.bstorm.formation.models.entities;

import be.bstorm.formation.models.BaseEntity;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Adresse extends BaseEntity<Long> {
    private String street;
}
