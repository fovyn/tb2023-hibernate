package be.bstorm.formation;

import be.bstorm.formation.daos.personne.PersonneRepository;
import be.bstorm.formation.di.DiContainer;
import be.bstorm.formation.models.entities.Personne;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.List;

@Slf4j
public class Main {
    public static void main(String[] args) throws IOException {
        DiContainer container = DiContainer.init();

        PersonneRepository dao = container.get(PersonneRepository.class);

        List<Personne> personnes = dao.findAll().toList();

        personnes.forEach(it -> log.info("{}", it));
        container.close();
    }
}