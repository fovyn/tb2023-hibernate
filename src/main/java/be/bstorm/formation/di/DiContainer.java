package be.bstorm.formation.di;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Persistence;
import org.reflections.Reflections;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

public class DiContainer implements Closeable {

    private final Map<Class<?>, Object> services = newHashMap();
    private EntityManager em;

    public static DiContainer init() {
        DiContainer $this = new DiContainer();
        Reflections reflections = new Reflections("be.bstorm.formation");
        Set<Class<?>> types = reflections.getTypesAnnotatedWith(Repository.class);

        $this.em = Persistence.createEntityManagerFactory("demo").createEntityManager();

        $this.services.put(EntityManager.class, $this.em);

        types.forEach($this::resolve);

        return $this;
    }

    private Object resolve(Class<?> clazz) {
        Class<?>[] tInterfaces = clazz.getInterfaces();
        Constructor<?> constructor = clazz.getConstructors()[0];

        Parameter[] parameters = constructor.getParameters();
        List<Object> values = newArrayList();
        for (Parameter parameter : parameters) {
            Object value = services.get(parameter.getType());

            if (value == null) {
                value = resolve(parameter.getType());
                services.put(parameter.getType(), value);
            }

            values.add(value);
        }

        try {
            Object instance = constructor.newInstance(values.toArray());
            for (Class<?> tInterface : tInterfaces) {
                services.put(tInterface, instance);
            }
            services.put(clazz, instance);
            return instance;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }


    public <T> T get(Class<T> clazz) {
        return (T) services.get(clazz);
    }


    @Override
    public void close() throws IOException {
        em.close();
    }
}
