package be.bstorm.formation.daos;

import be.bstorm.formation.models.BaseEntity;
import jakarta.persistence.EntityManager;
import lombok.Getter;

import java.io.Serializable;

public abstract class AbstractDao<T extends BaseEntity<TKey>, TKey extends Serializable> implements CrudRepository<T, TKey> {
    @Getter
    private final EntityManager em;

    protected AbstractDao(EntityManager em) {
        this.em = em;
    }
}
