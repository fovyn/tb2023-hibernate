package be.bstorm.formation.daos;

import be.bstorm.formation.models.BaseEntity;
import jakarta.persistence.EntityManager;

import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Stream;

public interface CrudRepository<T extends BaseEntity<TKey>, TKey extends Serializable> {
    EntityManager getEm();

    Stream<T> findAll();

    Optional<T> findById(TKey id);

    default void create(T obj) {
        this.getEm().getTransaction().begin();
        this.getEm().persist(obj);
        this.getEm().flush();
        this.getEm().getTransaction().commit();
    }

    default void update(T obj) {
        this.getEm().getTransaction().begin();
        this.getEm().merge(obj);
        this.getEm().getTransaction().commit();
    }

    default void remove(T obj) {
        this.getEm().getTransaction().begin();
        this.getEm().remove(obj);
        this.getEm().getTransaction().commit();
    }

    default void remove(TKey id) {
        this.getEm().getTransaction().begin();
        T obj = this.findById(id).orElseThrow();
        this.remove(obj);
        this.getEm().getTransaction().commit();
    }

    default void save(T obj) {
        if (obj.getId() != null) {
            this.update(obj);
        } else {
            this.create(obj);
        }
    }

}
