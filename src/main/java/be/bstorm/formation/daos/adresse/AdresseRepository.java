package be.bstorm.formation.daos.adresse;

import be.bstorm.formation.daos.CrudRepository;
import be.bstorm.formation.models.entities.Adresse;

public interface AdresseRepository extends CrudRepository<Adresse, Long> {
}
