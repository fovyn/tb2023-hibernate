package be.bstorm.formation.daos.adresse;

import be.bstorm.formation.daos.AbstractDao;
import be.bstorm.formation.di.Repository;
import be.bstorm.formation.models.entities.Adresse;
import jakarta.persistence.EntityManager;

import java.util.Optional;
import java.util.stream.Stream;

@Repository
public class AdresseDao extends AbstractDao<Adresse, Long> implements AdresseRepository {
    public AdresseDao(EntityManager em) {
        super(em);
    }

    @Override
    public Stream<Adresse> findAll() {
        return null;
    }

    @Override
    public Optional<Adresse> findById(Long id) {
        return Optional.empty();
    }
}
