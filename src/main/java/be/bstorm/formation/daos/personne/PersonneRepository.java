package be.bstorm.formation.daos.personne;

import be.bstorm.formation.daos.CrudRepository;
import be.bstorm.formation.models.entities.Personne;

import java.util.stream.Stream;

public interface PersonneRepository extends CrudRepository<Personne, Long> {

    Stream<Personne> findAllByLastname(String lastname);
}
