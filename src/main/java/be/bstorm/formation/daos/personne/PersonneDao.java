package be.bstorm.formation.daos.personne;

import be.bstorm.formation.daos.AbstractDao;
import be.bstorm.formation.daos.adresse.AdresseDao;
import be.bstorm.formation.di.Repository;
import be.bstorm.formation.models.entities.Adresse;
import be.bstorm.formation.models.entities.Personne;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;

import java.util.Optional;
import java.util.stream.Stream;

@Repository
public class PersonneDao extends AbstractDao<Personne, Long> implements PersonneRepository {

    private final AdresseDao adresseDao;

    public PersonneDao(EntityManager em, AdresseDao adresseDao) {
        super(em);
        this.adresseDao = adresseDao;
    }

    public Stream<Personne> findAll() {
        TypedQuery<Personne> query = getEm().createQuery("SELECT p FROM Personne p", Personne.class);

        return query.getResultStream();
    }

    public Optional<Personne> findById(Long id) {
        TypedQuery<Personne> query = getEm().createQuery("SELECT p FROM Personne p WHERE p.id = :id", Personne.class);

        query.setParameter("id", id);

        return Optional.of(query.getSingleResult());
    }

    @Override
    public Stream<Personne> findAllByLastname(String lastname) {
        TypedQuery<Personne> query = getEm().createQuery("SELECT p FROM Personne p WHERE p.lastname = :lastname", Personne.class);
        query.setParameter("lastname", lastname);

        return query.getResultStream();
    }

    @Override
    public void save(Personne entity) {
        for (Adresse adresse : entity.getAdresses()) {
            adresseDao.save(adresse);
        }
        super.save(entity);
    }
}
